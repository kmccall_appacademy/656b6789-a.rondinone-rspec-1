def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, rep=2)
  words = []
  rep.times do
  words << word
  end
  words.join(" ")
end

def start_of_word(word, num_of_letter)
  word.slice(0, num_of_letter)
end

def first_word(str)
  words = str.split(" ")
  words[0]
end

def titleize(str)
  str.capitalize!
  little_words = ["and", "or", "the", "over", "to", "the", "a", "but"]
  words = str.split(" ")
  title = []
  words.each do |word|
    if little_words.include?(word)
      title << word
    else
      title << word.capitalize
    end
  end
  title.join(" ")
end
