def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  sum = 0
  arr.each do |x|
    sum += x
  end
  sum
end

def multiply(num1, num2)
  num1 * num2
end

def power(base, power)
  base**power
end

def factorial(n)
  if n === 0
    return 0
  end

  fact = 1
  while n > 0
    fact *= n
    n -= 1
  end
  fact
end
