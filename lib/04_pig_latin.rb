
def vowel?(letter)
  vowels = "aeiouAEIOU".split("")
  vowels.include?(letter)
end

def if_vowel(word)
  word + "ay"
end

def if_consinent(word)
  i = 0
  while i < word.length
    letter = word[i]
    if letter == "q"
      i += 1
    elsif vowel?(letter)
      return word.slice(i..-1) + word.slice(0, i) + "ay"
    end
    i += 1
  end
  word + "ay"
end

def translate(str)
  words = str.split(" ")
  result = []
  words.each do |word|
    if vowel?(word[0])
      result << if_vowel(word)
    else
      result << if_consinent(word)
    end
  end
  result.join(" ")
end
