def ftoc(f_num)
  (f_num - 32) * 5 / 9.to_f
end

def ctof(c_num)
  (c_num * (9 / 5.to_f)) + 32
end
